from django.shortcuts import render_to_response
from django.template import RequestContext
from member.forms import MemberLoginForm, MemberRegistrationForm
from django.http import HttpResponseRedirect

def home(request):
    LoginForm = MemberLoginForm()
    RegistrationForm = MemberRegistrationForm()
    if request.user.is_authenticated():
        return HttpResponseRedirect("/member/dashboard/")
    return render_to_response("home.html",{'lform': LoginForm, 'rform': RegistrationForm}, context_instance=RequestContext(request))