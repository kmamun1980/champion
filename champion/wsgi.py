"""
WSGI config for champion project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys

for path in ['/root/ve/champion/lib/python2.7/site-packages', '/var/django/champion']:
    if path not in sys.path:
        sys.path.append(path)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "champion.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
