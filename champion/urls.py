from django.conf.urls import patterns, include, url
from django.contrib import admin
from djrill import DjrillAdminSite

admin.site = DjrillAdminSite()
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'champion.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^member/', include('member.urls')),
    #url(r'^campaign/', include('campaign.urls')),
    url(r'^mytwilio/', include('mytwilio.urls')),
    # url(r'^blog/', include('blog.urls')),
    url(r'^selectable/', include('selectable.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'media'}),

    url(r'^admin/', include(admin.site.urls)),
)

