from django.contrib import auth
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
# Create your views here.
from twilio.rest import TwilioRestClient
import twilio.twiml
from mytwilio.forms import PackageSearchForm, FilterTwilioNumberSearchForm
from member.forms import UserForm, MemberLoginForm, MemberRegistrationForm, MemberProfileForm, MemberTwilioNumberEditForm
from member.models import *

from urlparse import urlparse, parse_qs
from paypalrestsdk import Payment
import logging
import paypalrestsdk
from champion.utils import send_templated_email

PROFILE_CREATED = 'profile_created'
TWILIO_ACCONT_CREATED = 'twilio_account_created'
TWILIO_NUMBER_ADDED = 'twilio_number_added'
TWILIO_CONTACT_ADDED = 'twilio_contact_added'
SUSPENDED = 'suspended'
ACTIVE = 'active'




def test(request):
    #from string import Template
    import pdb; pdb.set_trace();
    
    # mtn = MemberTwilioNumber.objects.get(twilio_number='+14806668707', tw_account_sid='AC446d7ff204bc4824df3bd2ac7019ffba')

    account_sid = "AC2b684fe09a1ae4ad72d18c2ff54edac1"
    auth_token = "e8516d0e70a5c66de8ee57e4bf92d7bc"
    # master = TwilioRestClient(master_sid, master_auth_token)
    client = TwilioRestClient(account_sid, auth_token)
    #numbers = client.phone_numbers.list(phone_number="14803326999")
    caller_ids = client.caller_ids.list(phone_number="14803326999")
    # hostname = request.get_host()
    # resp = twilio.twiml.Response()
    # resp.dial('+14803326999', StatusCallback='http://'+hostname+'/mytwilio/outgoing-call/status/')
    # call = client.calls.create(url='http://'+hostname+'/mytwilio/handle_outgoing_call/',to='+14803326999', from_="+14803510021", 
    #             status_callback='http://'+hostname+'/mytwilio/outgoing-call/status/')
    # message = client.messages.create(body="Jenny please?! I love you <3",
    #     to="+8801743505740",
    #     from_="+14803510021",
    #     status_callback = 'http://www.procurementconnections.com/mytwilio/reply_message/status/')
    # resp = twilio.twiml.Response()
    # resp.message('I love django', to='+8801743505740', s<QueryDict: {u'FromZip': [u''], u'From': [u'+14803510021'], u'FromCity': [u''], u'ApiVersion': [u'2010-04-01'], u'To': [u'+14803326999'], u'ToCity': [u'PHOENIX'], u'CalledState': [u'AZ'], u'FromState': [u'AZ'], u'Direction': [u'outbound-api'], u'CallStatus': [u'in-progress'], u'ToZip': [u'85250'], u'CallerCity': [u''], u'FromCountry': [u'US'], u'CalledCity': [u'PHOENIX'], u'CalledCountry': [u'US'], u'Caller': [u'+14803510021'], u'CallerZip': [u''], u'AccountSid': [u'AC2b684fe09a1ae4ad72d18c2ff54edac1'], u'Called': [u'+14803326999'], u'CallerCountry': [u'US'], u'CalledZip': [u'85250'], u'CallSid': [u'CA02c201f00bd317dc165fd2425c3096a6'], u'CallerState': [u'AZ'], u'ToCountry': [u'US'], u'ToState': [u'AZ']}>ender='+14803510021',
    #     statusCallback='http://www.procurementconnections.com/mytwilio/reply_message/status/')
    
    return HttpResponse(call.sid)
    #numbers = client.phone_numbers.search(area_code="480", capabilities={"mms":"true",}, country="US", type="local")
    # site = request.META['HTTP_HOST']
    # status_callback_url = "http://"+ site + "/mytwilio/contact_number_verify/status_callback/"

    # caller_id = client.caller_ids.validate("+14803326999", status_callback = status_callback_url)
    # print caller_id.sid
    ''' to delete a subaccount user master '''
    #clos = master.accounts.update("ACa4915e17dc1a383b0bd5fc7ea9b27dcb", status="closed")

    return render_to_response("test.html",{'numbers': message.sid}, context_instance=RequestContext(request))


def contact_number(request):
    member_contact_numers = MemberContactNumber.objects.all()

    return render_to_response("member/contact-numbers.html", 
        {'contact_numbers': member_contact_numers}, 
            context_instance=RequestContext(request))


@csrf_exempt
def contact_number_add(request):
    
    if request.method == "POST":
                
        num = request.POST.get("contact-number", "")
        if num:
            if len(num) < 11:
                return HttpResponse("Not valid number")
            elif len(num) == 13:
                num = "+" + num

            
            user = request.user
            mp = MemberProfile.objects.get(member=user)
            account_sid = mp.tw_account_sid
            auth_token = mp.tw_auth_token
            client = TwilioRestClient(account_sid, auth_token)

            site = request.META['HTTP_HOST']
            status_callback_url = "http://"+ site + "/member/contact_number_verify/status_callback/"
            try:
                caller_id = client.caller_ids.validate(num, status_callback = status_callback_url)
            except:
                return HttpResponse("Error: Try again latter.")
            vcod = caller_id['validation_code']
            # vcod = '867739'
            return HttpResponse("Your veryfication code is: <b> %s </b>" % vcod)
        else:
            return HttpResponse("Not valid number")
    else:
        return HttpResponseRedirect("/member/contact_number/")

@csrf_exempt
def num_verify_status_callback(request):
    
    verification_status = request.POST.get("VerificationStatus")
    
    if verification_status == "success":
        contact_number = request.POST.get("To")
        outgoing_caller_id_sid = request.POST.get("OutgoingCallerIdSid")
        tw_account_sid = request.POST.get("AccountSid")
        member = MemberProfile.objects.get(tw_account_sid=tw_account_sid)

        try:
            MemberContactNumber.objects.create(
                member = member, 
                contact_number = contact_number,
                contact_number_sid = outgoing_caller_id_sid,
                is_verified = 1,
                is_active = 1,
                )
        except:
            pass
    else:
        return HttpResponseRedirect("/")


def home(request):
    LoginForm = MemberLoginForm()
    RegistrationForm = MemberRegistrationForm()
    return render_to_response("test.html",{'lform': LoginForm, 'rform': RegistrationForm}, context_instance=RequestContext(request))

@login_required
def staff_dashboard(request):
    members = MemberProfile.objects.all()
    return render_to_response("member/staff_dashboard.html", {}, 
        context_instance=RequestContext(request))

@login_required
def dashboard(request):
    
    user = request.user
    member_contacts = Contact.objects.filter(member=user)
    member_leads = Lead.objects.filter(contact__in=member_contacts)

    return render_to_response("member/dashboard.html", {'user': user,'member_leads': member_leads }, 
        context_instance=RequestContext(request))


@csrf_exempt
def member_register(request):
    if request.method == "POST":
        
        form = MemberRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Your registration is successfull. Please check your mail for activation link.")
            return HttpResponseRedirect("/")
        else:
            for error in form.errors:
                messages.error(request, form.errors[error])
            return HttpResponseRedirect("/")
    else:
        form = MemberRegistrationForm()
        
    return render_to_response("member/register.html", {'rform': form}, context_instance=RequestContext(request))

def member_activation_invalid(request):
    return render_to_response("member/member_activation_invalid.html", {}, context_instance=RequestContext(request))

def member_register_active(request, activation_code):
    try:
        ac = ActivationCode.objects.get(code=activation_code)
        if ac and ac.status == 0:
            user = ac.user
            user.is_active = True
            user.save()
            
            ac.delete()
            try:
                ''' Create a user profile '''
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                auth.login(request, user)
                return HttpResponseRedirect("/member/startup/")
                
            except:
                ''' Send an email to admin for creating an supplier for this user'''
                pass
            
    except ActivationCode.DoesNotExist:
        return HttpResponseRedirect("/member/activation/invalid/")    
    
@csrf_exempt
def member_login(request):
    if request.method == "POST":
        
        LoginForm = MemberLoginForm(request.POST)
            
        username = request.POST.get('username', "")
        password = request.POST.get('password', "")
        
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            if user.is_active and not user.is_staff:
                auth.login(request, user)
                try:
                    member_profile = MemberProfile.objects.get(member=user)
                    
                    if member_profile.profile_status not in (TWILIO_NUMBER_ADDED, TWILIO_CONTACT_ADDED, SUSPENDED, ACTIVE):
                        messages.error(request, "Please add your champion number to startup.")
                        return HttpResponseRedirect("/member/settings/")
                    elif member_profile.profile_status in (SUSPENDED, ):
                        messages.error(request, "Please contact admin (admin@champion.com).")
                        return HttpResponseRedirect("/member/profile/suspend/")

                    return HttpResponseRedirect("/member/dashboard/")

                except MemberProfile.DoesNotExist:
                    return HttpResponseRedirect("/member/startup/")
            elif user.is_active and user.is_staff:
                auth.login(request, user)
                return HttpResponseRedirect("/member/staff/dashboard/")
            else:
                messages.error(request, 'User %s is not activated yet. Please check your mail.' % username)
                return HttpResponseRedirect("/")
        else:
            messages.error(request, 'Username %s is not available in our system' % username)
            return HttpResponseRedirect("/")
    else:
        LoginForm = MemberLoginForm()
        
        return render_to_response("member/login.html", {'form': LoginForm}, 
                                  context_instance=RequestContext(request))

def member_logout(request):
    auth.logout(request);
    return HttpResponseRedirect("/")

@login_required
def member_startup(request):
    
    packages = Package.objects.filter(is_active=True, is_basic=True)
    # twform = FilterTwilioNumberSearchForm()
    return render_to_response("member/startup.html",{"packages": packages },
          context_instance=RequestContext(request))


@login_required
def member_package_subscribe(request):
    if request.method == "POST":
        package_id = request.POST.get('package_id',9999)
        package_frequency = request.POST.get('package_frequency', '')
        if package_frequency == '':
            package_frequency = 1
        else:
            package_frequency = int(package_frequency)

        # user_id = 1
        user = request.user
        if package_id != 9999:
            package = Package.objects.get(pk=package_id)
            unit_price = package.cost
            total_cost = unit_price * package_frequency
            transaction_name = package.title
            ''' String conversion for paypal support'''
            unit_price = str(unit_price)
            total_cost = str(total_cost)
            
        else:
            transaction_name = "Invalid Transaction"

        paypalrestsdk.configure({
            "mode": "sandbox", 
            "client_id": "AQW51xAPblqbEtHM_vxW7Kv_Mk2Y1lFVtr1YIhWkackcPDzzu4OJNE6WJnIz",
            "client_secret": "EJL20BC2_6Ktrk2uLBh3vFPz7hn3RWcmblmCIpjHYEGoGRq2Un7sP2c4-wR7" 
        })
        
        site = request.META['HTTP_HOST']
        payment = paypalrestsdk.Payment({
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://"+ site +"/member/payment/execute/",
                "cancel_url": "http://"+ site +"/member/payment/cancel/" 
            },
            "transactions": [{
                "item_list": {
                "items": [{
                    "name": transaction_name,
                    "sku": package_id,
                    "price": unit_price,
                    "currency": "USD",
                    "quantity": package_frequency }
                ]},
                "amount": {
                    "total": total_cost,
                    "currency": "USD" 
                },
                "description": "This is the payment transaction description." 
            }]
        })
        
        if payment.create():
            print("Payment[%s] created successfully"%(payment.id))
            ph = PaypalPaymentHistory.objects.create(member=user, package=package, payment_id=payment.id)

            ph.sms = package.sms * package_frequency
            ph.minute = package.talktime * package_frequency
            ph.mms = package.mms * package_frequency
            ph.email = package.email * package_frequency
            ph.attachement = package.digital_contant * package_frequency
            ph.cost = total_cost
            ph.twilio_number = package.twilio_number * package_frequency

            ph.status = 'created'
            ph.save()
            # Redirect the user to given approval url
            for link in payment.links:
                if link.method == "REDIRECT":
                    
                    redirect_url = link.href
                    query = parse_qs(urlparse(redirect_url).query)
                    token = query['token'][0]
                    ph.token = token
                    ph.save()
                    return redirect(redirect_url)
        else:
            messages.error(request, payment.error)
            return HttpResponseRedirect("/member/payment/error/")   

@login_required
def member_profile(request):
    if request.method == 'POST':
        pform = MemberProfileForm(request.POST)
        if pform.is_valid():
            pform.save()
            return "saved"
    else:
        pform = MemberProfileForm()

    return render_to_response("member/profile.html", {'pform': pform}, 
                                  context_instance=RequestContext(request))
    

@login_required
def member_settings(request, redirect_from="profile"):

    user = request.user
    memberprofile = MemberProfile.objects.get(member=user)
    
    ''' If profile is not created yet go to startup page'''
    try:
        if user.memberprofile:
            pass
    except:
        return HttpResponseRedirect("/member/startup/")
        

    if request.method == 'POST':
        pform = MemberProfileForm(request.POST, instance=memberprofile)
        uform = UserForm(request.POST, instance=user)

        if uform.is_valid() and pform.is_valid():
            user = uform.save(commit=False)
            user.save()
            profile = pform.save()

            messages.success(request, "Member profile saved successfully")
    else:
        uform = UserForm(instance=user)
        pform = MemberProfileForm(instance=memberprofile)

    tw_numbers = MemberTwilioNumber.objects.filter(member=user)

    twform = FilterTwilioNumberSearchForm()
    twnumberedit_form = MemberTwilioNumberEditForm()
    member_contact_numers = MemberContactNumber.objects.all()


    return render_to_response("member/settings.html", 
            {'uform': uform, 'pform': pform, "tw_numbers": tw_numbers, 'twform': twform,
             'twnumberedit_form': twnumberedit_form, 'path': redirect_from, 'contact_numbers': member_contact_numers},
            context_instance=RequestContext(request))


def tw_number_list(request):
    tw_numbers = MemberTwilioNumber.objects.filter(member=request.user)
    twform = FilterTwilioNumberSearchForm()
    twnumberedit_form = MemberTwilioNumberEditForm()
    return render_to_response("member/choose_number.html",{"tw_numbers": tw_numbers, 'twform': twform, 'twnumberedit_form': twnumberedit_form})


def member_register_success(request):
    return render_to_response("member/settings.html", {}, context_instance=RequestContext(request))
    
''' User FSM to update status'''
#@transition(source='created', target='executed')

def member_payment_execute(request):

    payer_id = request.GET['PayerID']
    token = request.GET['token']
    user = request.user
    pph = PaypalPaymentHistory.objects.get(token=token, member=user)
    package = pph.package

    paypalrestsdk.configure({
        "mode": "sandbox", 
        "client_id": "AQW51xAPblqbEtHM_vxW7Kv_Mk2Y1lFVtr1YIhWkackcPDzzu4OJNE6WJnIz",
        "client_secret": "EJL20BC2_6Ktrk2uLBh3vFPz7hn3RWcmblmCIpjHYEGoGRq2Un7sP2c4-wR7"
    })

    payment = paypalrestsdk.Payment.find(pph.payment_id)

    if payment.execute({"payer_id": payer_id}):
        print("Payment execute successfully")

        ''' Update paypal payment history status'''
        pph.status = 'executed'
        pph.save()

        ''' Update MemberCurrentBalance '''
        current_balance, test = MemberCurrentBalance.objects.get_or_create(member=user)
        current_balance.total_sms += pph.sms
        current_balance.total_minute += pph.minute
        current_balance.total_mms += pph.mms
        current_balance.total_email += pph.email
        current_balance.total_attachement += pph.attachement
        # current_balance.dollar_amount += pph.cost
        current_balance.purchased_twilio_number += pph.twilio_number
        current_balance.financial_status = 'healthy'
        current_balance.save()

        ''' Create member profile '''
        try:
            member_profile = MemberProfile.objects.get(member=user)

        except MemberProfile.DoesNotExist:
            member_profile = MemberProfile.objects.create(member=user)

        if member_profile.profile_status not in (TWILIO_ACCONT_CREATED, TWILIO_NUMBER_ADDED, TWILIO_CONTACT_ADDED, SUSPENDED, ACTIVE):
            ''' Create twilio subaccount '''
            tw_subaccount = member_profile.create_twilio_subaccount()
            

        return HttpResponseRedirect("/member/dashboard/")

    else:
        print(payment.error) # Error Hash
        messages.error(request, payment.error['message'])
        return HttpResponseRedirect("/member/dashboard/")


def member_payment_error(request):
    return HttpResponseRedirect("/")

def member_payment_cancel():
    return HttpResponseRedirect("/")
    pass

def member_recharge(request):

    packages = Package.objects.filter(is_active=True, is_basic=False)
    context = {
        "packages": packages
    }
    return render_to_response("member/recharge.html", context, context_instance=RequestContext(request))

def member_balance_info(request):
    user = request.user
    current_balance = MemberCurrentBalance.objects.get(member=user)
    minutes = current_balance.total_minute
    sms = current_balance.total_sms
    context = {
        'minutes': minutes,
        'sms': sms
    }
    return render_to_response("member/balance-info.html", context, context_instance=RequestContext(request))