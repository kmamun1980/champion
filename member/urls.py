from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^test/$', 'member.views.test', name='test'),
    url(r'^$', 'member.views.home', name='member-home'),
    url(r'^dashboard/$', 'member.views.dashboard', name='member-dashboard'),
    url(r'^staff/dashboard/$', 'member.views.staff_dashboard', name='member-staff-dashboard'),
    
    url(r'^register/$', 'member.views.member_register', name='member-register'),
    url(r'^register/success/$', 'member.views.member_register_success', name='member-register-success'),
    url(r'^register/(?P<activation_code>[\w]+)/active/$', 'member.views.member_register_active', name='member-register-active'),
    url(r'^activation/invalid/$', 'member.views.member_activation_invalid', name='member-activation-invalid'),
    
    
    url(r'^login/$', 'member.views.member_login', name='member-login'),
    url(r'^logout/$', 'member.views.member_logout', name='member-logout'),
    
    url(r'^startup/$', 'member.views.member_startup', name='member-startup'),
    url(r'^package/subscribe/$', 'member.views.member_package_subscribe', name='member-startup'),

    url(r'^recharge/$', 'member.views.member_recharge', name='member-recharge'),
    url(r'^payment/execute/$', 'member.views.member_payment_execute', name='member-payment-execute'),
    url(r'^payment/cancel/$', 'member.views.member_payment_cancel', name='member-payment-cancel'),
    url(r'^payment/error/$', 'member.views.member_payment_error', name='member-payment-error'),
    url(r'^balance-info/$', 'member.views.member_balance_info', name='member-balance-info'),

    url(r'^profile/$', 'member.views.member_profile', name='member-profile'),
    url(r'^settings/(?P<redirect_from>\w+)/$', 'member.views.member_settings', name='member-settings'),
    url(r'^settings/$', 'member.views.member_settings', name='member-settings'),
    url(r'^tw_number_list/$', 'member.views.tw_number_list', name='tw-number-list'),

    url(r'^contact_number/$', 'member.views.contact_number', name='contact_number'),
    url(r'^contact_number/add/$', 'member.views.contact_number_add', name='contact_number_add'),
    # url(r'^contact_number/edit/$', 'mytwilio.views.contact_number_edit', name='contact_number_edit'),
    # url(r'^contact_number/delete/$', 'mytwilio.views.contact_number_delete', name='contact_number_delete'),
    #url(r'^update_twnumber/(?P<tw_id>\d+)/$', 'mytwilio.views.update_twnumber', name='update_twilio_number'),
    url(r'^contact_number_verify/status_callback/$', 'member.views.num_verify_status_callback', name='num-verify-status-callback'),



)