from django.db import models
from django.contrib.auth.models import User
from django_fsm.db.fields import FSMField, transition, can_proceed
from django.contrib import messages
# Create your models here.
from django.dispatch import receiver
from django.db.models.signals import post_save
from datetime import datetime


class Industry(models.Model):
    industry_name = models.CharField(max_length=25)
    
class MemberProfile(models.Model):
    member = models.OneToOneField(User)
    industry = models.ForeignKey(Industry, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    # contact_number = models.CharField(max_length=15, blank=True, null=True)
    fax_number = models.CharField(max_length=15, blank=True, null=True)
    company_name = models.CharField(max_length=25, blank=True, null=True)
    designation = models.CharField(max_length=25, blank=True, null=True)
    tw_account_sid = models.CharField(max_length=50, blank=True, null=True)
    tw_auth_token = models.CharField(max_length=50, blank=True, null=True)
    
    PROFILE_CREATED = 'profile_created'
    TWILIO_ACCONT_CREATED = 'twilio_account_created'
    TWILIO_NUMBER_ADDED = 'twilio_number_added'
    TWILIO_CONTACT_ADDED = 'twilio_contact_added'
    SUSPENDED = 'suspended'
    ACTIVE = 'active'

    profile_status = FSMField(default=PROFILE_CREATED)

    @transition(source=PROFILE_CREATED, target=TWILIO_ACCONT_CREATED, save=True)
    def create_twilio_subaccount(self):

        from mytwilio.views import create_subaccount
        try:
                
            tw_subaccount = create_subaccount(self.member.username)
            self.tw_account_sid = tw_subaccount.sid
            self.tw_friendly_name = self.member.email
            self.tw_auth_token = tw_subaccount.auth_token
            self.twilio_account_created = True
            self.save()
            return tw_subaccount

        except:
            ''' There must have a way to create twilio subaccount '''
            messages.error(self.request, 'Your twilio account is not created. Please contact at admin@champion.com')
            return HttpResponseRedirect("/member/settings/")

    @transition(source=(TWILIO_ACCONT_CREATED, TWILIO_NUMBER_ADDED, ), target=TWILIO_NUMBER_ADDED, save=True)
    def add_twilio_number(self, twnumber, twnumber_sid):

      user = self.member
      current_balance, test = MemberCurrentBalance.objects.get_or_create(member=user)

      mtn = MemberTwilioNumber.objects.create(member=user)
      mtn.tw_account_sid = self.tw_account_sid
      mtn.twilio_number = twnumber.phone_number
      mtn.twilio_number_sid = twnumber_sid
      mtn.sms_enabled = twnumber.capabilities['SMS']
      mtn.mms_enabled = twnumber.capabilities['MMS']
      mtn.voice_enabled = twnumber.capabilities['voice']

      mtn.country_code = twnumber.iso_country
      mtn.save()

      current_balance.total_added_tw_number += 1
      current_balance.save()

    def __unicode__(self):
        return self.member.username

class ActivationCode(models.Model):
    user = models.ForeignKey(User)
    code = models.CharField(max_length=200, unique=True)
    ACTIVATION_STATUS = ((1,"ACTIVE"), (2,"INACTIVE"), (0,"PENDING"))
    status = models.IntegerField(choices=ACTIVATION_STATUS)
    CODE_FOR = (("PASSWORD_RESET","PASSWORD_RESET"), ("USER_REGISTER","USER_REGISTER"))
    code_for = models.CharField(max_length=20, choices=CODE_FOR, blank=True, null=True)
    

class Package(models.Model):
    title = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField(default=False)
    is_basic = models.BooleanField(default=False)
    cost = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, default=0.0)
    sms = models.IntegerField(blank=True, null=True, default=0)
    talktime = models.IntegerField(blank=True, null=True, default=0)
    mms = models.IntegerField(blank=True, null=True, default=0)
    email = models.IntegerField(blank=True, null=True, default=0)
    digital_contant = models.IntegerField(blank=True, null=True, default=0)
    is_twilio_number_included = models.BooleanField(default=False)
    twilio_number = models.IntegerField(default=0, blank=True, null=True)
    note = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.title
    
    
class PaypalPaymentHistory(models.Model):
    member = models.ForeignKey(User)
    package = models.ForeignKey(Package)
    sms = models.IntegerField(blank=True, null=True, default=0)
    minute = models.IntegerField(blank=True, null=True, default=0)
    mms = models.IntegerField(blank=True, null=True, default=0)
    email = models.IntegerField(blank=True, null=True, default=0)
    attachement = models.IntegerField(blank=True, null=True, default=0)
    twilio_number = models.IntegerField(blank=True, null=True, default=0)
    # prefered_areacode = models.CharField(max_length=3, null=True, blank=True)
    token = models.CharField(max_length=60, blank=True, null=True)
    cost = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True, default=0.0)
    status = FSMField(default='created')
    datetime = models.DateTimeField(default=datetime.now(),blank=True)
    payment_id = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.member.username + " / " + self.payment_id


@receiver(post_save, sender=PaypalPaymentHistory)
def my_callback(sender, **kwargs):
    print("Request finished!")

class MemberCurrentBalance(models.Model):
    member = models.OneToOneField(User)
    total_sms = models.IntegerField(blank=True, null=True, default=0)
    total_mms = models.IntegerField(blank=True, null=True, default=0)
    total_minute = models.IntegerField(blank=True, null=True, default=0)
    total_email = models.IntegerField(blank=True, null=True, default=0)
    total_attachement = models.IntegerField(blank=True, null=True, default=0)
    total_added_tw_number = models.IntegerField(blank=True, null=True, default=0)
    purchased_twilio_number = models.IntegerField(blank=True, null=True, default=0)
    available_tw_number_to_purchase = models.IntegerField(blank=True, null=True, default=0)
    financial_status = FSMField(default='invalid')

    def get_total_sms(self):
        return self.total_sms

    def get_total_minute(self):
        return self.total_minute

    def get_total_mms(self):
        return self.total_mms

    def get_total_email(self):
        return self.total_email

    def __unicode__(self):
        return "%s have %d sms %d minutes" % (self.member.username, self.total_sms, self.total_minute)

    def available_tw_number_to_purchase(self):
        return self.purchased_twilio_number - self.total_added_tw_number

    def save(self, *args, **kwargs ):
        try:
            self.available_tw_number_to_purchase = self.purchased_twilio_number - self.total_added_tw_number
        except:
            self.available_tw_number_to_purchase = 0
            pass
        super(MemberCurrentBalance, self).save(*args, **kwargs)


class MemberContactNumber(models.Model):
    member = models.ForeignKey(MemberProfile)
    contact_number = models.CharField(max_length=15, blank=True, null=True)
    contact_number_sid = models.CharField(max_length=50, blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.contact_number




class MemberTwilioNumber(models.Model):
    member = models.ForeignKey(User)
    tw_account_sid = models.CharField(max_length=50, blank=True, null=True)
    twilio_number = models.CharField(max_length=15, blank=True, null=True)
    twilio_number_sid = models.CharField(max_length=35, blank=True, null=True)
    tw_auth_token = models.CharField(max_length=50, blank=True, null=True)
    friendly_name = models.CharField(max_length=50, blank=True, null=True)
    area_code = models.IntegerField(blank=True, null=True)
    country_code = models.CharField(max_length=3, blank=True, null=True)  
    sms_enabled = models.BooleanField(default=1)
    mms_enabled = models.BooleanField(default=0)
    voice_enabled = models.BooleanField(default=1)
    note = models.TextField(blank=True, null=True)
    active_sms_reply = models.BooleanField(default=False)
    message_body = models.TextField(max_length=200, blank=True, null=True)
    sms_reply_email_active = models.BooleanField(default=False)
    mms_reply_active = models.BooleanField(default=False)
    mms_file = models.FileField(upload_to="mms", blank=True, null=True)
    sms_reply_email_body = models.TextField(blank=True, null=True)
    sms_reply_email_attachement = models.FileField(upload_to="sms_reply_email_attachement", blank=True, null=True)
    active_voice_reply = models.BooleanField(default=False)
    vc_reply_text2speach = models.TextField(blank=True, null=True)
    is_playback_mp3 = models.BooleanField(default=False)
    vc_playback_mp3 = models.FileField(upload_to="mp3", blank=True, null=True)
    vc_redirect = models.BooleanField(default=False)
    vc_redirect_to = models.ForeignKey(MemberContactNumber, blank=True, null=True)


    ''' overwrite save to auto update twilio_number field if area_code and number field is inserted'''
    def __unicode__(self):
        return self.member.username + " / " + self.twilio_number




@receiver(post_save, sender=MemberTwilioNumber)
def twilio_number_added(sender, **kwargs):
    print "Twilio number added to %s account." % sender.member
    print "Update country code, area code, "
    # import pdb; pdb.set_trace()
    #member = sender.member.MemberProfilephone_number
    pass

class Campaign(models.Model):
    pass



class Contact(models.Model):
    member = models.ForeignKey(User)
    contact_number = models.CharField(max_length=15, blank=True, null=True)
    contact_fname = models.CharField(max_length=50, blank=True, null=True)
    contact_lname = models.CharField(max_length=50, blank=True, null=True)
    contact_email = models.CharField(max_length=50, blank=True, null=True)
    company_name = models.CharField(max_length=50, blank=True, null=True)
    designation = models.CharField(max_length=50, blank=True, null=True)  
    note = models.TextField(blank=True, null=True)

    class Meta:
        unique_together = ('member', 'contact_number',)

    def __unicode__(self):
        return self.member.username + " / " + self.contact_number


class Lead(models.Model):
    contact = models.ForeignKey(Contact)
    campaign = models.ForeignKey(Campaign, blank=True, null=True)
    # name and email is redundant and should be removed
    name = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)

    call_sid = models.CharField(max_length=50, blank=True, null=True)
    called = models.CharField(max_length=50, blank=True, null=True)
    sms_sid = models.CharField(max_length=50, blank=True, null=True)
    LEAD_TYPE = (("sms","SMS"),("mms","MMS"), ("voice","VOICE"))
    lead_type = models.CharField(max_length=10, choices=LEAD_TYPE, blank=True, null=True)
    lead_sms = models.CharField(max_length=300, blank=True, null=True)
    lead_voice = models.CharField(max_length=300, blank=True, null=True)
    voice_record_sid = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.contact.member.username + " / " + self.contact.contact_number