# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Industry'
        db.create_table(u'member_industry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('industry_name', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'member', ['Industry'])

        # Adding model 'MemberProfile'
        db.create_table(u'member_memberprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.Industry'], null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('fax_number', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('designation', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('tw_account_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('tw_auth_token', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('profile_status', self.gf('django_fsm.db.fields.fsmfield.FSMField')(default='profile_created', max_length=50)),
        ))
        db.send_create_signal(u'member', ['MemberProfile'])

        # Adding model 'ActivationCode'
        db.create_table(u'member_activationcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('status', self.gf('django.db.models.fields.IntegerField')()),
            ('code_for', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['ActivationCode'])

        # Adding model 'Package'
        db.create_table(u'member_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_basic', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cost', self.gf('django.db.models.fields.DecimalField')(default=0.0, null=True, max_digits=7, decimal_places=2, blank=True)),
            ('sms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('talktime', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('mms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('digital_contant', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('is_twilio_number_included', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('twilio_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['Package'])

        # Adding model 'PaypalPaymentHistory'
        db.create_table(u'member_paypalpaymenthistory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.Package'])),
            ('sms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('minute', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('mms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('attachement', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('twilio_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=60, null=True, blank=True)),
            ('cost', self.gf('django.db.models.fields.DecimalField')(default=0.0, null=True, max_digits=7, decimal_places=2, blank=True)),
            ('status', self.gf('django_fsm.db.fields.fsmfield.FSMField')(default='created', max_length=50)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 1, 12, 0, 0), blank=True)),
            ('payment_id', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['PaypalPaymentHistory'])

        # Adding model 'MemberCurrentBalance'
        db.create_table(u'member_membercurrentbalance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('total_sms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('total_mms', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('total_minute', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('total_email', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('total_attachement', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('total_added_tw_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('purchased_twilio_number', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('financial_status', self.gf('django_fsm.db.fields.fsmfield.FSMField')(default='invalid', max_length=50)),
        ))
        db.send_create_signal(u'member', ['MemberCurrentBalance'])

        # Adding model 'MemberContactNumber'
        db.create_table(u'member_membercontactnumber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.MemberProfile'])),
            ('contact_number', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('contact_number_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('is_verified', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'member', ['MemberContactNumber'])

        # Adding model 'MemberTwilioNumber'
        db.create_table(u'member_membertwilionumber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('tw_account_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('twilio_number', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('twilio_number_sid', self.gf('django.db.models.fields.CharField')(max_length=35, null=True, blank=True)),
            ('tw_auth_token', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('friendly_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('area_code', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('country_code', self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True)),
            ('sms_enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('mms_enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('voice_enabled', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('active_sms_reply', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('message_body', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('sms_reply_email_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mms_reply_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mms_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('sms_reply_email_body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('sms_reply_email_attachement', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('active_voice_reply', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vc_reply_text2speach', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('is_playback_mp3', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vc_playback_mp3', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('vc_redirect', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('vc_redirect_to', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.MemberContactNumber'], null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['MemberTwilioNumber'])

        # Adding model 'Campaign'
        db.create_table(u'member_campaign', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'member', ['Campaign'])

        # Adding model 'Contact'
        db.create_table(u'member_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('contact_number', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('contact_fname', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('contact_lname', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('contact_email', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('designation', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['Contact'])

        # Adding unique constraint on 'Contact', fields ['member', 'contact_number']
        db.create_unique(u'member_contact', ['member_id', 'contact_number'])

        # Adding model 'Lead'
        db.create_table(u'member_lead', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.Contact'])),
            ('campaign', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['member.Campaign'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('call_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('called', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('sms_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('lead_type', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('lead_sms', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('lead_voice', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('voice_record_sid', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'member', ['Lead'])


    def backwards(self, orm):
        # Removing unique constraint on 'Contact', fields ['member', 'contact_number']
        db.delete_unique(u'member_contact', ['member_id', 'contact_number'])

        # Deleting model 'Industry'
        db.delete_table(u'member_industry')

        # Deleting model 'MemberProfile'
        db.delete_table(u'member_memberprofile')

        # Deleting model 'ActivationCode'
        db.delete_table(u'member_activationcode')

        # Deleting model 'Package'
        db.delete_table(u'member_package')

        # Deleting model 'PaypalPaymentHistory'
        db.delete_table(u'member_paypalpaymenthistory')

        # Deleting model 'MemberCurrentBalance'
        db.delete_table(u'member_membercurrentbalance')

        # Deleting model 'MemberContactNumber'
        db.delete_table(u'member_membercontactnumber')

        # Deleting model 'MemberTwilioNumber'
        db.delete_table(u'member_membertwilionumber')

        # Deleting model 'Campaign'
        db.delete_table(u'member_campaign')

        # Deleting model 'Contact'
        db.delete_table(u'member_contact')

        # Deleting model 'Lead'
        db.delete_table(u'member_lead')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'member.activationcode': {
            'Meta': {'object_name': 'ActivationCode'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'code_for': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'member.campaign': {
            'Meta': {'object_name': 'Campaign'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'member.contact': {
            'Meta': {'unique_together': "(('member', 'contact_number'),)", 'object_name': 'Contact'},
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'contact_fname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'contact_lname': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'designation': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'member.industry': {
            'Meta': {'object_name': 'Industry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry_name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'member.lead': {
            'Meta': {'object_name': 'Lead'},
            'call_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'called': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'campaign': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.Campaign']", 'null': 'True', 'blank': 'True'}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.Contact']"}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_sms': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'lead_type': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'lead_voice': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sms_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'voice_record_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'member.membercontactnumber': {
            'Meta': {'object_name': 'MemberContactNumber'},
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'contact_number_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.MemberProfile']"})
        },
        u'member.membercurrentbalance': {
            'Meta': {'object_name': 'MemberCurrentBalance'},
            'financial_status': ('django_fsm.db.fields.fsmfield.FSMField', [], {'default': "'invalid'", 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'purchased_twilio_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_added_tw_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_attachement': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_email': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_minute': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_mms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'total_sms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        u'member.memberprofile': {
            'Meta': {'object_name': 'MemberProfile'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'designation': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'fax_number': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.Industry']", 'null': 'True', 'blank': 'True'}),
            'member': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'profile_status': ('django_fsm.db.fields.fsmfield.FSMField', [], {'default': "'profile_created'", 'max_length': '50'}),
            'tw_account_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'tw_auth_token': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'member.membertwilionumber': {
            'Meta': {'object_name': 'MemberTwilioNumber'},
            'active_sms_reply': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'active_voice_reply': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'area_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'country_code': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'friendly_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_playback_mp3': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'message_body': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'mms_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mms_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mms_reply_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'sms_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sms_reply_email_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sms_reply_email_attachement': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'sms_reply_email_body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'tw_account_sid': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'tw_auth_token': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'twilio_number': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'twilio_number_sid': ('django.db.models.fields.CharField', [], {'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'vc_playback_mp3': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'vc_redirect': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vc_redirect_to': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.MemberContactNumber']", 'null': 'True', 'blank': 'True'}),
            'vc_reply_text2speach': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'voice_enabled': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'member.package': {
            'Meta': {'object_name': 'Package'},
            'cost': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'null': 'True', 'max_digits': '7', 'decimal_places': '2', 'blank': 'True'}),
            'digital_contant': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_basic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_twilio_number_included': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'sms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'talktime': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'twilio_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        u'member.paypalpaymenthistory': {
            'Meta': {'object_name': 'PaypalPaymentHistory'},
            'attachement': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'null': 'True', 'max_digits': '7', 'decimal_places': '2', 'blank': 'True'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 1, 12, 0, 0)', 'blank': 'True'}),
            'email': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'minute': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'mms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['member.Package']"}),
            'payment_id': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sms': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'status': ('django_fsm.db.fields.fsmfield.FSMField', [], {'default': "'created'", 'max_length': '50'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'twilio_number': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['member']