from selectable.base import ModelLookup, LookupBase
from member.models import Package
from selectable.registry import registry

class PackageLookup(ModelLookup):
	model = Package
	search_fields = ('title__icontains',)

registry.register(PackageLookup)