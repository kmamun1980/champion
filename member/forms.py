from django import forms
from champion.utils import send_templated_email
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.sites.models import Site
import uuid
from md5 import md5

from member.models import ActivationCode, MemberProfile, MemberTwilioNumber


class MemberLoginForm(forms.Form):
    username = forms.CharField(max_length=25, 
                               required=True, 
                               label="User Name",
                               widget=forms.TextInput(attrs={"class": "input"}))
    
    password = forms.CharField(max_length=25,
                               required=True,
                               label="Password", 
                               widget=forms.PasswordInput(render_value=True, attrs={"class": "input"}))


class MemberRegistrationForm(UserCreationForm):
    username = forms.CharField(max_length=25, 
                               required=True, 
                               label="User Name",
                               widget=forms.TextInput(attrs={"class": "input"}))
    email = forms.EmailField(label="Email Address", widget=forms.EmailInput(attrs={"class": "input"}))
    password1 = forms.CharField(max_length=25,
                               required=True,
                               label="Password", 
                               widget=forms.PasswordInput(render_value=True, attrs={"class": "input"}))
    
    password2 = forms.CharField(max_length=25,
                               required=True,
                               label="Password", 
                               widget=forms.PasswordInput(attrs={"class": "input"}))
    
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
        
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.set_password(self.cleaned_data['password1'])
        user.is_active = False
        #testmail = send_mail("registration", "activation link", "mamun1980@gmail.com", [user.email,], fail_silently=False)
        
        activation_code = md5(str(uuid.uuid4())).hexdigest()
        
        current_site = Site.objects.get_current()
        
        HOSTNAME = current_site.domain
        url = "http://" + HOSTNAME + "/member/register/" + activation_code + "/active/"
            
        if commit:
            
            try:
                from string import Template
                user.save()
                ActivationCode.objects.create(user=user,code=activation_code, status=0)
              #   email_body_tmpl = Template(mtn.sms_reply_email_body)
              #   email_body = email_body_tmpl.substitute(name=sender_name)
                send_templated_email(
                    subject = "Champion email confirmation",
                    email_template_name='templated_email/champemail_reg.html',
                    sender = 'admin@champion.com',
                    recipients=user.email,

                    email_context={
                        'activation_url': url,
                    }
                )
                
            except:
                pass
        return user

class UserForm(forms.ModelForm):
  first_name = forms.CharField(max_length=25,
                               required=False,
                               label="First Name",
                               widget=forms.TextInput(attrs={"class": "input"}))
  last_name = forms.CharField(max_length=25, 
                               required=False, 
                               label="Last Name",
                               widget=forms.TextInput(attrs={"class": "input"}))
  email = forms.CharField(max_length=50,
                               required=True,
                               label="Eamil",
                               widget=forms.EmailInput(attrs={"class": "input"}))


  class Meta:
    model = User
    fields = ['first_name', 'last_name', 'email']

  def save(self, commit=True):
    user = super(UserForm, self).save(commit=False)
    user.first_name = self.cleaned_data['first_name']
    user.last_name = self.cleaned_data['last_name']
    user.email = self.cleaned_data['email']
    user.save()
    return user



class MemberProfileForm(forms.ModelForm):
  
  address = forms.CharField(max_length=25, 
                            required=False,
                            label="Address",
                            widget=forms.Textarea(attrs={"class": ""}))
  fax_number = forms.CharField(max_length=15, 
                               required=False, 
                               label="Fax Number",
                               widget=forms.TextInput(attrs={"class": "input"}))
  company_name = forms.CharField(max_length=20, 
                               required=False, 
                               label="Company Name",
                               widget=forms.TextInput(attrs={"class": "input"}))

  class Meta:
    model = MemberProfile
    exclude = ['member', 'tw_account_sid', 'tw_auth_token', 'tw_account_status', 'profile_complated',
               'contact_number_added', 'designation', 'tw_friendly_name']
    fields = ['address', 'fax_number', 'company_name']

  def save(self, commit=True):

    profile = super(MemberProfileForm, self).save(commit=False)
    profile.address = self.cleaned_data['address']
    profile.company_name = self.cleaned_data['company_name']
    profile.fax_number = self.cleaned_data['fax_number']
    profile.save()
    return profile

class MemberTwilioNumberEditForm(forms.ModelForm):
  message_body = forms.CharField(max_length=200, 
                               required=False, 
                               label="Write your messages",
                               widget=forms.Textarea(attrs={"class": "input edittwnumber", 'rows':3, 'cols': 25}))
  sms_reply_email_body = forms.CharField(max_length=200, 
                               required=False, 
                               label="Write your email here",
                               widget=forms.Textarea(attrs={"class": "input edittwnumber", 'rows':3,  'cols': 25}))
  note = forms.CharField(max_length=200, 
                               required=False, 
                               label="Put some notes",
                               widget=forms.Textarea(attrs={"class": "input edittwnumber", 'rows':3,  'cols': 25}))
  vc_reply_text2speach = forms.CharField(max_length=200,
                               required=False,
                               label="Text to speach",
                               widget=forms.Textarea(attrs={"class": "input edittwnumber", 'rows':3,  'cols': 25}))
  class Meta:
    model = MemberTwilioNumber
    exclude = ['member', 'tw_account_sid', 'twilio_number', 'twilio_number_sid', 'friendly_name',
              'area_code', 'country_code', 'sms_enabled', 'mms_enabled', 'voice_enabled']
    