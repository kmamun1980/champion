from django.contrib import admin
from member.models import *
# Register your models here.

admin.site.register(ActivationCode)
admin.site.register(Industry)
admin.site.register(MemberProfile)
admin.site.register(Package)
admin.site.register(PaypalPaymentHistory)
admin.site.register(MemberCurrentBalance)
admin.site.register(MemberTwilioNumber)
admin.site.register(MemberContactNumber)
admin.site.register(Contact)
admin.site.register(Lead)
