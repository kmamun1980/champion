from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^test/$', 'mytwilio.views.test_email', name='mytwilio_test'),
    url(r'^reply_message/$', 'mytwilio.views.reply_message', name='reply-message'),

    url(r'^handle_outgoing_call/$', 'mytwilio.views.outgoing_call_handle', name='handle-outgoing-call'),
    url(r'^outgoing-call/status/$', 'mytwilio.views.outgoing_call_status', name='reply-outgoing-call-status'),
    url(r'^handle_incomming_call/$', 'mytwilio.views.handle_incomming_call', name='handle-incomming-call'),
    url(r'^handle-recording/$', 'mytwilio.views.handle_recording', name='handle-recording'),

    url(r'^handle-key/$', 'mytwilio.views.handle_key', name='handle_key'),
    # url(r'^get_numbers/(?P<area_code>[\d]{3})/$', 'mytwilio.views.get_numbers_by_areacode', name='mytwilio-number-by-code'),
    url(r'^search_numbers_by_ac/$', 'mytwilio.views.search_numbers_by_areacode', name='mytwilio-search-number'),
    url(r'^search_numbers/$', 'mytwilio.views.search_numbers', name='tw-search-number'),
    
    url(r'^buynumber/$', 'mytwilio.views.buynumber', name='buy_twilio_number'),
    url(r'^buynumbers/$', 'mytwilio.views.buynumbers', name='buy_twilio_numbers'),

    url(r'^update_twnumber/(?P<tw_id>\d+)/$', 'mytwilio.views.update_twnumber', name='update_twilio_number'),
    
)