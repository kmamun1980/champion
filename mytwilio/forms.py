from django import forms
from member.models import Package
import selectable
from lookups import *


class PackageSearchForm(forms.Form):
	# search = selectable.forms.AutoCompleteWidget(PackageLookup, label='', required=False)
	autocomplete = forms.CharField(
        label='',
        widget=selectable.forms.AutoCompleteWidget(PackageLookup, attrs={"class": "input", "placeholders":"Enter your prefered number"}),
        required=False,

    )

class FilterTwilioNumberSearchForm(forms.Form):
	area_code = forms.CharField(max_length=3, 
                               required=False, 
                               label="Area Code",
                               widget=forms.TextInput(attrs={"class": "input"}))

	country = forms.CharField(max_length=3, 
                               required=False, 
                               label="Country",
                               widget=forms.TextInput(attrs={"class": "input"}))
	# sms_enabled = forms.BooleanField()
	# mms_enabled = forms.BooleanField()
	# voice_enabled = forms.BooleanField()