from django.shortcuts import render, render_to_response
# from django.template.loader import render_to_string
# from django.contrib import messages
from champion.utils import send_templated_email
from twilio.rest import TwilioRestClient
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
from django.template.response import TemplateResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse

from member.models import *
from member.forms import MemberTwilioNumberEditForm
import twilio.twiml

ACCOUNT_SID = "AC5ae463aab85b52f5b540969dff416712"
AUTH_TOKEN = "005085c7b1e2662551418ffc213ac996"

# DSV import
from django.core.mail import EmailMessage
import mimetypes


def test_email(request):
    import pdb; pdb.set_trace();
    from django.template import Template, loader, Context
    
    email_body = 'I love my son'
    
    tmpl = loader.get_template('templated_email/reply_message_email.html')
    # email_body_tmpl = Template(tmpl)
    con = Context({"name": "Mamun", "body": email_body})
    email_content = tmpl.render(con)
    
    
    email = EmailMessage(
        'Email from champion', 
        email_content, 'admin@champion.com', 
        ['mamun1980@gmail.com',], 
        headers = {'Reply-To': 'admin@champion.com'}
    )
    email.send()


# Create your views here.
def home(request):
  account_sid = ACCOUNT_SID
  auth_token = AUTH_TOKEN
  client = TwilioRestClient(account_sid, auth_token)
  return render_to_response("twilio-home.html", {'msg': message})


@csrf_exempt
def reply_message(request):
    
    tw_number_from = request.POST.get("From", '+8801743505740')
    account_sid = request.POST.get("AccountSid",'AC8792a347f8748e735fa0b70a790e5974') # 'ACee27bb0ac9c27566f6b4ed74a44fbc4c') #dev
    tw_number_to = request.POST.get("To",'+14805259964') # "+14807254656") #dev
    
    message_body = request.POST.get("Body",'Mamun mamun1980@gmail.com')
    sms_sid = request.POST.get("SmsSid", "CA22ac0234c1892e9b7c9cd327bdea9533")

    mtn = MemberTwilioNumber.objects.get(twilio_number=tw_number_to, tw_account_sid=account_sid)
    user = mtn.member
    auth_token = mtn.tw_auth_token
    site = request.META['HTTP_HOST']

    client = TwilioRestClient(account_sid, auth_token)

    if message_body:
        # sms = Template(mtn.message_body)
        sms = mtn.message_body
        # sms_body = sms.substitute(name=sender_name, email=sender_email)
        
        messages = message_body.split(" ")
        sender_email = messages[-1]

        try:
            contact = Contact.objects.get(contact_number=tw_number_from)
            lead = Lead.objects.create(contact=contact)
        except:
            contact = Contact.objects.create(member=user, contact_number=tw_number_from, contact_email=sender_email)
            lead = Lead.objects.create(contact=contact)
            pass

        
        if len(messages) == 3:
            sender_fname = messages[0]
            sender_lname = messages[1]
            contact.contact_fname = sender_fname
            contact.contact_lname = sender_lname
        else:
            sender_fname = messages[0]
            contact.contact_fname = sender_fname

        # save contact and lead

        contact.save()
        lead.lead_type = 'sms'
        lead.lead_sms = messages
        lead.email = sender_email
        lead.name = sender_fname
        lead.sms_sid = sms_sid
        lead.called = tw_number_to
        try:
            lead.save()
            
        except:
            pass


        # Prepare sms body
        sms_body = "Hi "+sender_fname + ", " + sms
           
    else: 
        sms_body = mtn.message_body

    if mtn.active_sms_reply:
        sms_to_send = sms_body
        if user.membercurrentbalance.total_sms > 0:
            message = client.messages.create(
                to=tw_number_from, 
                from_=tw_number_to, 
                body=sms_body,
                # status_callback = 'http://'+ site +'/mytwilio/reply_message/status/'
            )

            if message.sid:
                mcb = MemberCurrentBalance.objects.get(member=user)
                mcb.total_sms = mcb.total_sms -1
                mcb.save()

        else:
            message = client.messages.create(
                to=tw_number_to, 
                from_='', 
                body='You sms balance is empty. Please recharge.'                
            )


    # if mtn.mms_reply_active:
    #     mms_url = mtn.mms_file.path
    #     mms_body = "This is MMS"

    #     if user.membercurrentbalance.total_mms > 0:
    #         message = client.messages.create(
    #             to=tw_number_from, 
    #             from_=tw_number_to, 
    #             body=mms_body,
    #             # status_callback = 'http://'+ site +'/mytwilio/reply_message/status/'
    #         )
    #         if message.sid:
    #             mcb = MemberCurrentBalance.objects.get(member=user)
    #             mcb.total_sms = mcb.total_sms -1
    #             mcb.save()
    #     else:
    #         message = client.messages.create(
    #             to=tw_number_to, 
    #             from_='', 
    #             body='You mms balance is empty. Please recharge.'                
    #         )

    if mtn.sms_reply_email_active:
        from django.template import Template, loader, Context
        try:
            email_body = mtn.sms_reply_email_body
            attach = mtn.sms_reply_email_attachement.path
            
        except:
            attach = None
            pass
        if user.membercurrentbalance.total_email > 0:
            tmpl = loader.get_template('templated_email/reply_message_email.html')
            con = Context({"name": sender_fname, "body": email_body})
            email_content = strip_tags(tmpl.render(con))
            email = EmailMessage(
                'Email from champion', 
                email_content, 'admin@champion.com', 
                [sender_email,], 
                headers = {'Reply-To': 'admin@champion.com'}
            )
            if attach:

                f = open(attach)
                content_type = mimetypes.guess_type(attach)[0]
                email.attach(f.name, f.read(), content_type)
                email.send()
            else:
                email.send()

        elif  user.membercurrentbalance.total_email <= 0:
                send_templated_email(
                    subject = "No balance for email",
                    email_template_name='templated_email/no-balance-for-digital-content.html',
                    sender = 'admin@champion.com',
                    recipients=user.email,
                    #files = attach,
                    email_context={
                        'body': 'You have no balance for digital content',
                    },
                )
        
    return HttpResponse(message.sid)


@csrf_exempt
def handle_incomming_call(request):
    
    
    to = request.POST.get("To", "+14808450662")
    from_number = request.POST.get('From', '+8801743505740')
    asid = request.POST.get("AccountSid", "AC9ad2f891cb8406494f83f30d0a25b05c")
    resp = twilio.twiml.Response()

    try:
        mtn = MemberTwilioNumber.objects.get(twilio_number=to, tw_account_sid=asid)
        auth_token = mtn.tw_auth_token

    except Exception, e:
        resp.say("No member is associated with this number")
        return HttpResponse(resp, mimetype='application/xml')

    if mtn.active_voice_reply:        
        
        if mtn.is_playback_mp3 and mtn.vc_playback_mp3:
            host = request.get_host()
            url = "http://" + host + mtn.vc_playback_mp3.url
            resp.play(url)
            
        if mtn.vc_reply_text2speach != "":
            speach = mtn.vc_reply_text2speach
            
            resp.say(speach)
        
        resp.gather(numDigits=1, action="/mytwilio/handle-key/", method="POST")
        
    else:
        resp.say("Voice call to this number is not allowed.")

    return HttpResponse(resp, mimetype='application/xml')

@csrf_exempt
def handle_key(request):
    to = request.POST.get("To", "+14803510021")
    from_number = request.POST.get('From', '+8801743505740')
    account_sid = request.POST.get("AccountSid", "AC2b684fe09a1ae4ad72d18c2ff54edac1")
    resp = twilio.twiml.Response()

    try:
        mtn = MemberTwilioNumber.objects.get(twilio_number=to, tw_account_sid=account_sid)
        client = TwilioRestClient(account_sid, mtn.tw_auth_token)
    except Exception, e:
        pass

    if request.method == "POST":
        digit_pressed = request.POST.get('Digits')
    else:
        digit_pressed = '1'
    
    if digit_pressed == "1":

        if mtn.vc_redirect:
            try:
                redirect_to = mtn.vc_redirect_to.contact_number
                # redirect_to = redirect_to[1:]
            except:
                resp.say("Sorry! No number has beet set yet.")

            hostname = request.get_host()
            resp.dial(redirect_to, StatusCallback='http://'+hostname+'/mytwilio/outgoing-call/status/')
            # call = client.calls.create(url='http://'+hostname+'/mytwilio/handle_outgoing_call/',
            #     to=redirect_to, from_=to, 
            #     status_callback='http://'+hostname+'/mytwilio/outgoing-call/status/')


        # If the dial fails:
        # resp.say("The call failed, or the remote party hung up. Goodbye.")
        return HttpResponse(resp, mimetype='application/xml')
        
    elif digit_pressed == "2":
        
        resp.say("Record your monkey howl after the tone.")
        resp.record(maxLength="30", action="/mytwilio/handle-recording/")
        return HttpResponse(resp, mimetype='application/xml')
    else:
        resp.say("Thanks for your calling. Goodbye.")
        return HttpResponse(resp, mimetype='application/xml')


@csrf_exempt
def outgoing_call_handle(request):
    
    resp = twilio.twiml.Response()
    to = request.POST.get("To")
    from_number = request.POST.get('From')
    resp.dial(to)
    return HttpResponse(resp, mimetype='application/xml')

@csrf_exempt
def outgoing_call_status(request):
    resp = twilio.twiml.Response()
    if request.POST.get("CallStatus") == 'completed':
        to = request.POST.get("To")
        from_number = request.POST.get('From')
        account_sid = request.POST.get('AccountSid')
        call_duration = request.POST.get('CallDuration')
        call_sid = request.POST.get('CallSid')
        mtn = MemberTwilioNumber.objects.get(twilio_number=from_number, tw_account_sid=account_sid)
        user = mtn.member
        mcb = MemberCurrentBalance.objects.get(member=user)
        mcb.total_minute -= call_duration
        mcb.save()
        return HttpResponse(resp, mimetype='application/xml')
        
    else:
        print request.POST.get("CallStatus")
    return HttpResponse(resp, mimetype='application/xml')

@csrf_exempt
def handle_recording(request):
    
    to = request.POST.get("To", "+14808450662")
    from_number = request.POST.get('From', '+8801743505740')
    asid = request.POST.get("AccountSid", "AC9ad2f891cb8406494f83f30d0a25b05c")
    resp = twilio.twiml.Response()

    try:
        mtn = MemberTwilioNumber.objects.get(twilio_number=to, tw_account_sid=asid)

    except Exception, e:
        pass


    user = mtn.member
    try:
        contact = Contact.objects.get(member=user, contact_number=from_number)
        lead = Lead.objects.create(contact=contact)
        
    except:
        contact = Contact.objects.create(member=user, contact_number=from_number)
        lead = Lead.objects.create(contact=contact)
        pass
    
    """Play back the caller's recording."""
    recording_url = request.POST.get("RecordingUrl", None)
    lead.lead_type = 'voice'
    lead.lead_voice = recording_url
    lead.call_sid = request.POST.get("CallSid", None)
    lead.voice_record_sid = request.POST.get("RecordingSid", None)
    lead.save()
    
    resp.say("Goodbye")
    return HttpResponse(resp, mimetype='application/xml')

@csrf_exempt
def search_numbers_by_areacode(request):
  if request.method == "POST":
    areacode = request.POST.get('area_code', 510)
    country_code = request.POST.get('country_code', "US")
    account_sid = ACCOUNT_SID
    auth_token = AUTH_TOKEN
    client = TwilioRestClient(account_sid, auth_token)
    numbers = []
    try:
      numbers = client.phone_numbers.search(area_code=areacode, country="US", type="local")
      return render_to_response("mytwilio/tw_search_result.html",{'numbers': numbers})
    except:
      return render_to_response("mytwilio/tw_search_result.html",{'numbers': numbers})

def create_subaccount(friendly_name):
  account_sid = ACCOUNT_SID
  auth_token = AUTH_TOKEN
  client = TwilioRestClient(account_sid, auth_token)
  account = client.accounts.create(friendly_name=friendly_name)
  return account

def purchage_twilio_number(twilio_number, account_sid, auth_token):
  client = TwilioRestClient(account_sid, auth_token)
  number = client.phone_numbers.purchase(phone_number=twilio_number)
  return number

def purchage_tw_number_by_areacode(areacode, account_sid, auth_token):
    client = TwilioRestClient(account_sid, auth_token)
    try:
        numbers = client.phone_numbers.search(area_code=areacode, country="US", type="local", capabilities={"mms":"true",})
        if numbers:
              numbers[0].purchase()
              details = client.phone_numbers.list(phone_number=numbers[0].phone_number)
              sid = details[0].sid
              hostname = request.get_host()
              client.phone_numbers.update(sid, voice_url="http://"+ hostname +"/mytwilio/handle_incomming_call/",
              sms_url="http://"+ hostname +"/mytwilio/reply_message/")
              return numbers[0], sid
    except:
        return False


def purchage_random_tw_number(account_sid, auth_token):
    client = TwilioRestClient(account_sid, auth_token)
    try:
        numbers = client.phone_numbers.search()
        if numbers:
            numbers[0].purchase()
            details = client.phone_numbers.list(phone_number=numbers[0].phone_number)
            sid = details[0].sid
            hostname = request.get_host()
            client.phone_numbers.update(sid, voice_url="http://"+ hostname +"/mytwilio/handle_incomming_call/",
                        sms_url="http://"+ hostname +"/mytwilio/reply_message/")
            return numbers[0], sid
    except:
        return False

@csrf_exempt
@login_required
def buynumber(request):
    if request.method == 'POST':
        tm = request.POST.get('twnumber', '')
        if tm:

            user = request.user
            member = MemberProfile.objects.get(member=user)
            account_sid = member.tw_account_sid
            auth_token = member.tw_auth_token
            client = TwilioRestClient(account_sid, auth_token)
            try:
                twnumber = client.phone_numbers.purchase(phone_number=tm)
                if not member.twilio_number_added:
                    member.twilio_number_added = True

                ''' Update member current balance '''
                current_balance, test = MemberCurrentBalance.objects.get_or_create(member=user)
                current_balance.total_added_tw_number += 1
                current_balance.save()

                ''' Update MemnerTwilioNumber '''
                mtn = MemberTwilioNumber.objects.create(member=user)
                mtn.twilio_number = twnumber.phone_number
                mtn.twilio_number_sid = twnumber.sid
                mtn.tw_auth_token = auth_token
                mtn.sms_enabled = twnumber.capabilities['sms']
                mtn.mms_enabled = twnumber.capabilities['mms']
                mtn.voice_enabled = twnumber.capabilities['voice']
                # mtn.area_code = pph.prefered_areacode
                # mtn.country_code = twnumber.iso_country
                mtn.save()

                messages.success(request, "New twilio number has been added to your account.")
                return HttpResponse("Your purchase is successfull")
            except:
                return HttpResponse("Error in purchase")
        else:
            return HttpResponse("No number found")

@csrf_exempt
@login_required
def buynumbers(request):
    if request.method == "POST":
        choices = request.POST.getlist("mychoices")
        user = request.user
        member = MemberProfile.objects.get(member=user)
        account_sid = member.tw_account_sid
        auth_token = member.tw_auth_token
        client = TwilioRestClient(account_sid, auth_token)
        purchased_number = []

        current_balance = MemberCurrentBalance.objects.get(member=user)
        twnumber_balance = current_balance.available_tw_number_to_purchase()

        if twnumber_balance == 0:
            messages.info(request, "You have no balance to buy champion number. Please recharge to buy.")
            return HttpResponseRedirect("/member/recharge/")
        elif len(choices) > twnumber_balance:
            choices = choices[:twnumber_balance]


        for number in choices:
            try:
                twnumber = client.phone_numbers.purchase( phone_number=number )
                details = client.phone_numbers.list(phone_number=twnumber.phone_number)
                twnumber_sid = details[0].sid
                hostname = request.get_host()
                client.phone_numbers.update(twnumber_sid, voice_url="http://" + hostname + "/mytwilio/handle_incomming_call/",
                        sms_url="http://" + hostname + "/mytwilio/reply_message/")

                purchased_number.append(twnumber)

                ''' Update member current balance '''
                current_balance, test = MemberCurrentBalance.objects.get_or_create(member=user)
                current_balance.total_added_tw_number += 1
                current_balance.save()

                ''' Update MemnerTwilioNumber '''
                mtn = MemberTwilioNumber.objects.create(member=user)
                mtn.tw_account_sid = account_sid
                mtn.twilio_number = twnumber.phone_number
                mtn.twilio_number_sid = twnumber_sid
                mtn.tw_auth_token = auth_token
                mtn.sms_enabled = twnumber.capabilities['sms']
                mtn.mms_enabled = twnumber.capabilities['mms']
                mtn.voice_enabled = twnumber.capabilities['voice']
                # mtn.area_code = pph.prefered_areacode
                # mtn.country_code = twnumber.iso_country
                mtn.save()

            except:
                pass

        return HttpResponseRedirect("/member/settings/")
        # tw_numbers = MemberTwilioNumber.objects.filter(member=user)
        # context_instance = RequestContext(request, {'pjax': request.META.get('HTTP_X_PJAX'), 'tw_numbers': tw_numbers})
        # return TemplateResponse(request, 'mytwilio/tw-number-list.html', context_instance)
    else:
        return HttpResponseRedirect("/member/settings/")


@csrf_exempt
def update_twnumber(request, tw_id=""):
    mtn = MemberTwilioNumber.objects.get(pk=tw_id)

    if request.method == "GET":
        twnumber_editform = MemberTwilioNumberEditForm(instance=mtn)
        return render_to_response("mytwilio/twnumberedit_formtpm.html",
                                  {"twnumberedit_form": twnumber_editform, "tw_id": tw_id},  context_instance=RequestContext(request))

    elif request.method == "POST":
        
        tw_editform = MemberTwilioNumberEditForm(request.POST, request.FILES, instance=mtn)
        if tw_editform.is_valid():
            tw_editform.save()

            # twid = request.POST.get("twid",'')
            # note = request.POST.get("note",'')
            # active_sms_reply = request.POST.get("active_sms_reply", '')
            # message_body = request.POST.get("message_body", '')
            # sms_reply_email_active = request.POST.get("sms_reply_email_active", '')
            # sms_reply_email_body = request.POST.get("sms_reply_email_body", '')
            #
            # mtn = MemberTwilioNumber.objects.get(pk=twid)
            #
            # if active_sms_reply == "true":
            #     mtn.active_sms_reply = True
            # else:
            #     mtn.active_sms_reply = False
            #
            # mtn.message_body = message_body
            # if sms_reply_email_active == 'false':
            #     mtn.sms_reply_email_active = False
            # else:
            #     mtn.sms_reply_email_active = True
            #
            # mtn.sms_reply_email_body = sms_reply_email_body
            #
            # mtn.note = note
            # mtn.save()
            #twnumber_editform = MemberTwilioNumberEditForm(request.POST)
            messages.success(request, "Twilio number updated successfully")
        redirect_url = reverse("member-settings", kwargs={'redirect_from': 'choose_number' })
        return HttpResponseRedirect(redirect_url)


def search_numbers(request):
	
    if request.method == 'POST':

        areacode = request.POST.get("area_code","")
        country = request.POST.get("country", "")

        account_sid = ACCOUNT_SID
        auth_token = AUTH_TOKEN
        client = TwilioRestClient(account_sid, auth_token)
        numbers = []
        try:
            numbers = client.phone_numbers.search(area_code=areacode, country="US", type="local")

        except:
            pass

        context_instance = RequestContext(request, {'pjax': request.META.get('HTTP_X_PJAX'), 'numbers': numbers})
        return TemplateResponse(request, 'mytwilio/tw_search_result.html', context_instance)
    else:
        return HttpResponseRedirect("/member/settings/")
